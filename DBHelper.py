import sqlite3
from datetime import datetime

class DBHelper:

    def __init__(self, path):
        self.conn = sqlite3.connect(path)
        self.cursor = self.conn.cursor()

    def close(self):
        self.conn.commit()
        self.conn.close()

    def createVideoInfoTable(self):
        sql = "CREATE TABLE if NOT EXISTS video_info (id INTEGER PRIMARY KEY AUTOINCREMENT, token TEXT, name TEXT, videopath TEXT)"
        try:
            self.cursor.execute(sql)
        except Exception as e:
            print("[warning] {}".format(e))

    def insertVideoInfoTable(self, video_info):
        table_name = "video_info"
        sql = 'INSERT INTO {} (token, name, videopath) VALUES ("{}", "{}", "{}")'.format(table_name, video_info['token'], video_info['name'], video_info["videopath"])

        try:
            self.cursor.execute(sql)
        except Exception as e:
            print("[warning] {}".format(e))

    def createEventTable(self, name):
        table_name = "F{}_event_r".format(name) # prefix can't be number
        sql = "CREATE TABLE {} (id INTEGER PRIMARY KEY AUTOINCREMENT, eventID INT, s_frame INT, e_frame TEXT, title TEXT, source INT, serialID INT, UpdateTime DATETIME DEFAULT CURRENT_TIMESTAMP)".format(table_name)
        
        try:
            self.cursor.execute(sql)
        except Exception as e:
            print('[createEvebTable] {}'.format(e))

    def insertEvent(self, name, event):
        table_name = "F{}_event_r".format(name)
        sql = 'INSERT INTO {} (eventID, s_frame, e_frame, title, source, serialID) VALUES ("{}","{}","{}","{}","{}","{}")'.format(table_name, event['eventID'], event['s_frame'], event['e_frame'], event['title'], event['source'], event['serialID'])
       
        try:
            self.cursor.execute(sql)
        except Exception as e:
            print('[insertEvent] {}'.format(e))

    def createTrackerTable(self, name):
        table_name = "F{}_tracker".format(name)
        sql = "CREATE TABLE {} (id INTEGER PRIMARY KEY AUTOINCREMENT, tracker INT, frame INT, labelName TEXT, position1x INT, position1y INT, position2x INT, position2y INT, center1x INT, center1y INT, realx FLOAT, realy FLOAT, angle INT, output INT, UpdateTime DATETIME DEFAULT CURRENT_TIMESTAMP)".format(table_name)

        try:
            self.cursor.execute(sql)
        except Exception as e:
            print("[createTrackerTable] {}".format(e))

    def insertTracker(self, name, trackers):
        table_name = "F{}_tracker".format(name)
        sql = "INSERT INTO {} (tracker, frame, labelName, position1x, position1y, position2x, position2y, center1x, center1y, realx, realy, angle, output) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)".format(table_name)
        
        rows = []
        for tracker in trackers:
            tracker['UpdateTime'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            
            row = (tracker['tracker'], tracker['frame'], tracker['labelName'], tracker['position1x'], tracker['position1y'], tracker['position2x'], tracker['position2y'], tracker['center1x'], tracker['center1y'], tracker['realx'], tracker['realy'], tracker['angle'], tracker['output'])
            rows.append(row)
        try:
            self.cursor.executemany(sql, rows)
        except Exception as e:
            print("[insertTracker] {}".format(e))

def test():
    db = DBHelper("example.db")
    db.createEventTable("20190826143229")
    #db.createTrackerTable("test")
    
    db.close()


if __name__ == '__main__':
    test()
