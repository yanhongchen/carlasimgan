# Carla sim gan
基於Carla模擬器的自動化交通事件生成工具，提供開發者快速、自動產生多型態交通模擬事件標記資料的Python腳本介面。 生成訓練object detection, muti-object tracking的標記資料
### 功能
1. 基礎車輛行為紀錄/重播
2. 再現車輛行為，並自動微調行為細節
3. 模擬大量微調後的車輛行為，依需求擷取需要的模擬成果，產生標記資料
4. 單一事件多視角資料
5. 自動驗證生成資料品質

## 安裝方式
```shell
cd path/to/caral/PythonAPI/example
git clone https://yanhongchen@bitbucket.org/yanhongchen/carlasimgan.git
```

## 使用範例
```python
python3 my_logreplayer.py 20190826143229.csv --epochs 200 --offset_actor_id 114 --collision_type RightAngleCollision --anchor_name traffic.speed_limit.90
```
- epochs: log重播的次數
- offset_actor_id: 輸入你要調整offset的actor的id
- collision_type: 基礎事件的類別（RightAngleCollision: 交叉撞, LeftTurnCollision: 左轉側撞）
- anchor_name: 攝影機架設的錨點物件

## Todo
- [ ] 增加邊開手動開車邊紀錄並且生成sqlite資料的程式
- [ ] 解決bounding box 有時不準的問題
- [ ] 摩擦係數調整
- [ ] 油門深度限制調整