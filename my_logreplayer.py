import glob
import os
import sys
import math
import datetime

try:
    path = os.path.join('..', '..', 'carla', 'dist', 'carla-*%d.%d-%s.egg' % (
        sys.version_info.major,
        sys.version_info.minor,
        'win-amd64' if os.name == 'nt' else 'linux-x86_64'))
    sys.path.append(glob.glob(path)[0])
except IndexError:
    pass
# ==============================================================================
# -- imports -------------------------------------------------------------------
# ==============================================================================


import carla
from carla import ColorConverter as cc

import time
import csv
import weakref
import threading
import numpy as np
import cv2
import xml.etree.ElementTree as ET

from BoundingBox import ClientSideBoundingBoxes
from ReplayData import ReplayData
from Observer import Observer
from DBHelper import DBHelper

class Replayer:
    def __init__(self, server_ip, replaydata, output_dir, db_path, collisionType='LeftTurnCollision', anchor_name='traffic.speed_limit.90'):
        """
        1. parse data generate task config file
        2. create client
        3. server env settings
        """
        self.collisionType = collisionType #"RightAngleCollision" "LeftTurnCollision"
        self.output_dir = output_dir
        self.db = None

        self.spawned_actors = {}

        # Parse config file
        self.data = replaydata
        self.data_path = self.data.path

        # create client
        port = 2000
        self.carla_client = carla.Client(server_ip, port)
        self.carla_client.set_timeout(12.0)
        self.world = self.carla_client.get_world()
        self.world.tick()

        # server env settings
        self.anchors = []
        self._find_spectator()
        self._set_multi_spectator(anchor_name)

        settings = self.world.get_settings()
        settings.synchronous_mode = True
        self.world.apply_settings(settings)

        self.observers = []

        self.width = 1280 
        self.height = 720
        self.fov = 100 

        self.server_frame = None
        self.collision_sensor = []

        self.collision = False
        self.finish = False

        self.event_info = {}
        self.collision_events = {}
        #self.frame_id = None


        self.solid_actors = []

        self.prepare_to_save(db_path)

    def prepare_to_save(self, db_path="./carlasimgan.db"):
        
        self.db = DBHelper(db_path)


    def create_cameras(self, postprocess, transform):
        calibration = np.identity(3)
        calibration[0, 2] = self.width / 2.0
        calibration[1, 2] = self.height / 2.0
        calibration[0, 0] = calibration[1, 1] = self.width / (2.0 * np.tan(self.fov * np.pi / 360.0))

        # create rgb
        bp_library = self.world.get_blueprint_library()
        camera_bp = bp_library.find('sensor.camera.rgb')
        camera_bp.set_attribute('image_size_x', str(self.width))
        camera_bp.set_attribute('image_size_y', str(self.height))
        camera_bp.set_attribute('fov', str(self.fov))
        camera_bp.set_attribute('enable_postprocess_effects', str(postprocess))

        rgb_camera = self.world.spawn_actor(camera_bp, transform)
        rgb_camera.calibration = calibration

        # create seg
        bp_library = self.world.get_blueprint_library()
        camera_bp = bp_library.find('sensor.camera.semantic_segmentation')
        camera_bp.set_attribute('image_size_x', str(self.width))
        camera_bp.set_attribute('image_size_y', str(self.height))
        camera_bp.set_attribute('fov', str(self.fov))

        seg_camera = self.world.spawn_actor(camera_bp, transform)
        seg_camera.calibration = calibration

        return rgb_camera, seg_camera

    def setup_collision_sensor(self, parent):
        bp = self.world.get_blueprint_library().find('sensor.other.collision')
        sensor = self.world.spawn_actor(bp, carla.Transform(), attach_to=parent)
        weak_self = weakref.ref(self)
        sensor.listen(lambda event: self._on_collision(weak_self, event))
        self.collision_sensor.append(sensor)

    @staticmethod
    def _on_collision(weak_self, event):
        self = weak_self()
        if not self:
            return
        #self.collision = True
        if event.other_actor.id == 0:
            #return
            print('[INFO] {}'.format(event))
        else:
            print('[INFO] {}'.format(event))
            self.collision = True
            # log this collision
            self.collision_events[str(event.actor.id)] = {'other_actor_id': event.other_actor.id}
            self.collision_events[str(event.other_actor.id)] = {'other_actor_id': event.actor.id}
    
    def _find_spectator(self):
        
        try:
            self.spectator = self.world.get_actors().filter('spectator')[0]
        except Exception as e:
            settings = self.world.get_settings()
            settings.synchronous_mode = True
            self.world.apply_settings(settings)
            self.world.wait_for_tick()
            self.spectator = self.world.get_actors().filter('spectator')[0]

        #print("[INFO]", "Find spectator:", self.spectator)

    def _set_multi_spectator(self, anchor_name):
        """
        set mutlti spectator for multicamera application
        """
        print("[INFO] current actors {}".format(self.world.get_actors()))
        self.anchors = []
        
        for sl in self.world.get_actors().filter(anchor_name):
            self.anchors.append(sl.get_transform())

        print("[INFO] anchors {}".format(self.anchors))

    def set_observers(self):
        index = 1
        for anchor in self.anchors:
            transform = anchor
            transform.location.z += 3
            rgb_camera, seg_camera = self.create_cameras(True, transform)
            rgb_camera.set_transform(transform)
            seg_camera.set_transform(transform)
            
            video_fn = "{}_{}.avi".format(self.collisionType, index)
            video_path = os.path.join(self.output_dir, video_fn)
            video_options = {"height": self.height, "width": self.width, "filename": video_path}

            csv_options = {'filename': os.path.join(self.output_dir, "output{}.csv".format(index))}
            observer = Observer(rgb_camera, seg_camera, video_options, csv_options)

            self.observers.append(observer)
            index += 1

    def play(self):
        """
        start simulating car events
        """
        self.world.tick()
        self.set_observers()
        
        # Start replaying

        server_start_time = None
        is_event_happend = False
        collision_interval = 0
        start_frame_id = None
        frame_id = 0 
        try:
            while self.finish is not True:

                self.world.tick()
                timestamp = self.world.wait_for_tick()
                
                #self.frame_id = timestamp.frame_count
                server_time = timestamp.elapsed_seconds

                if server_start_time is None:
                    #self.event_info['s_frame'] = self.frame_id
                    server_start_time = server_time

                # get current time
                current_time = server_time - server_start_time
                #print('[DEBUG] current time: {}/{}'.format(current_time, self.data.end_elapsed_time))

                if self.data.end_elapsed_time < current_time:
                    # no collision happen during this log
                    print("[INFO] {} no collision".format(self.output_dir))
                    #self.writeEventToDB()
                    self.finish = True

                self.collision = False
                # go next step
                self.step_forward(current_time)

                # logging
                actors = [a for a in self.spawned_actors.values()]
                for i, observer in enumerate(self.observers):
                    if start_frame_id is None:
                        start_frame_id = timestamp.frame_count

                    frame_id = timestamp.frame_count - start_frame_id

                    for actor in actors:
                        observer.capture(actor, frame_id)
                
                if self.collision and is_event_happend is not True:
                    self.event_info['s_frame'] = frame_id 
                    is_event_happend = True
                    collision_interval = 0
                else:
                    if is_event_happend:
                        collision_interval += 1
                    if is_event_happend and collision_interval >= 10 and len(self.collision_events.keys()) > 0:
                        self.finish = True
                        print('[INFO] Finish, collision_interval: {}'.format(collision_interval))
                        self.event_info['e_frame'] = frame_id 

                        # write event only if exists
                        self.writeEventToDB()

        except Exception as e:
            print("[WARNING] {}".format(e))
        except KeyboardInterrupt as e:
            print("[WARNING] {}".format(e))
        
        self.cleanup()

        if self.db is not None:
            self.db.close()

    def writeEventToDB(self):
        """
        """
        print("[INFO] write to db")
        print("[INFO] saving {}".format(self.output_dir))

        # create dir
        if not os.path.isdir(self.output_dir):
            os.makedirs(self.output_dir)

        # Save logs and video if collision happend
        for index, observer in enumerate(self.observers):
            name = os.path.basename(self.output_dir)[:-4]
            name = name.replace('.', "_")
            name = "{}_{}".format(name, index)

            self.db.createVideoInfoTable()
            video_info = {}
            video_info["token"] = name 
            video_info["videopath"] = os.path.basename(observer.video_options['filename'])
            video_info["name"] = os.path.splitext(video_info["videopath"])[0]

            self.db.insertVideoInfoTable(video_info)

            self.db.createTrackerTable(name)
            for k in observer.logs:
                logs = observer.logs[k]
                self.db.insertTracker(name, logs)
            #observer.writeCSV()
            observer.writeVideo()
        
            self.db.createEventTable(name)

            for aid in self.collision_events.keys():
                event = self.collision_events[aid]

                other_aid = event['other_actor_id']

                event_log = {}

                event_log['s_frame'] = max(self.event_info['s_frame'] - 75, 0)
                event_log['e_frame'] = self.event_info['e_frame']

                event_log['eventID'] = "{}_{}".format(aid, other_aid)
                event_log['title'] = self.collisionType #"RightAngleCollision" "LeftTurnCollision"
                event_log['source'] = 1
                event_log['serialID'] = aid
                print(f'[info] insert event: {name} {event_log}')
                self.db.insertEvent(name, event_log)

    def add_solid_actor(self, aid):
        self.solid_actors.append(aid)

    def step_forward(self, current_time):
        """
        """
        pair_dict = {}
        commands = []
        for actor_id in self.data.df["actor_id"].unique():
            pair_dict[actor_id] = {}
    
            # spawn actor if actor not exists
            if actor_id not in self.spawned_actors.keys():
                # set transform 
                pair_dict[actor_id]["transform"] = carla.Transform()
                pair_dict[actor_id]["transform"].location.x = self.data.get_actor_property(actor_id, current_time, 'x')
                pair_dict[actor_id]["transform"].location.y = self.data.get_actor_property(actor_id, current_time, 'y')
                pair_dict[actor_id]["transform"].location.z = self.data.get_actor_property(actor_id, current_time, 'z')+0.5
                
                pair_dict[actor_id]["transform"].rotation.pitch = self.data.get_actor_property(actor_id, current_time, 'pitch')
                pair_dict[actor_id]["transform"].rotation.yaw = self.data.get_actor_property(actor_id, current_time, 'yaw')
                pair_dict[actor_id]["transform"].rotation.roll = self.data.get_actor_property(actor_id, current_time, 'roll')

                #pair_dict[actor_id]["transform"].location.z += 1.0
                # set car type
                actor_type = self.data.get_actor_type(actor_id)
                blueprint = self.world.get_blueprint_library().filter(actor_type)[0]

                # set role name
                role_name = self.data.get_actor_role_name(actor_id)
                blueprint.set_attribute('role_name', role_name)

                # set color
                color_r, color_g, color_b = self.data.get_actor_color(actor_id)
                blueprint.set_attribute('color', ",".join([str(color_r), str(color_g), str(color_b)]))        
                
                # setup vehicle
                print("actor_id: {}, transform: {}".format(actor_id, pair_dict[actor_id]['transform']))
                self.spawned_actors[actor_id] = self.world.spawn_actor(blueprint, pair_dict[actor_id]['transform'])
                
                # setup collision sensor
                self.setup_collision_sensor(self.spawned_actors[actor_id])

            if str(actor_id) in self.solid_actors:
                # set transform 
                pair_dict[actor_id]["transform"] = carla.Transform()
                pair_dict[actor_id]["transform"].location.x = self.data.get_actor_property(actor_id, current_time, 'x')
                pair_dict[actor_id]["transform"].location.y = self.data.get_actor_property(actor_id, current_time, 'y')
                #pair_dict[actor_id]["transform"].location.z = self.data.get_actor_property(actor_id, current_time, 'z')
                
                pair_dict[actor_id]["transform"].rotation.pitch = self.data.get_actor_property(actor_id, current_time, 'pitch')
                pair_dict[actor_id]["transform"].rotation.yaw = self.data.get_actor_property(actor_id, current_time, 'yaw')
                pair_dict[actor_id]["transform"].rotation.roll = self.data.get_actor_property(actor_id, current_time, 'roll')

                commands.append(carla.command.ApplyTransform(self.spawned_actors[actor_id], pair_dict[actor_id]['transform']))

                # set velocity
                pair_dict[actor_id]["velocity"] = carla.Vector3D()
                pair_dict[actor_id]["velocity"].x = self.data.get_actor_property(actor_id, current_time, 'velocity_x')
                pair_dict[actor_id]["velocity"].y = self.data.get_actor_property(actor_id, current_time, 'velocity_y')
                pair_dict[actor_id]["velocity"].y = self.data.get_actor_property(actor_id, current_time, 'velocity_z')


            # set control
            pair_dict[actor_id]['control'] = carla.VehicleControl()
            pair_dict[actor_id]['control'].throttle = self.data.get_actor_control(actor_id, current_time, 'throttle')
            pair_dict[actor_id]['control'].steer = self.data.get_actor_control(actor_id, current_time, 'steer')
            pair_dict[actor_id]['control'].brake = self.data.get_actor_control(actor_id, current_time, 'brake')
            pair_dict[actor_id]['control'].hand_brake = bool(self.data.get_actor_control(actor_id, current_time, 'hand_brake') > 0.0)
            pair_dict[actor_id]['control'].reverse = bool(self.data.get_actor_control(actor_id, current_time, 'reverse') > 0.0)
            pair_dict[actor_id]['control'].manual_gear_shift = bool(self.data.get_actor_control(actor_id, current_time, 'manual_gear_shift') > 0.0)
            pair_dict[actor_id]['control'].gear = int(self.data.get_actor_control(actor_id, current_time, 'gear'))
            #print("[DEBUG] actor {} command {}".format(self.spawned_actors[actor_id], pair_dict[actor_id]['control']))
            commands.append(carla.command.ApplyVehicleControl(self.spawned_actors[actor_id], pair_dict[actor_id]['control']))
            
        if self.finish is not True:
            self.carla_client.apply_batch(commands)

    def cleanup(self):
        settings = self.world.get_settings()
        settings.synchronous_mode = False
        self.world.apply_settings(settings)

        for aid in self.spawned_actors:
            actor = self.spawned_actors[aid]
            actor.destroy()

        for sensor in self.collision_sensor:
            sensor.destroy()

        for obs in self.observers:
            obs.destroy()
    
    def test(self):
        self.play()

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('csv_path')
    
    parser.add_argument('--epochs', default=1, type=int)
    parser.add_argument('--offset_actor_id', default=None, help='actor needs to set offset') #114
    parser.add_argument('--time_offset_range', default=0, type=float, help='Maxium of offset value')
    parser.add_argument('--db_path', default='carlasimgan.db', help="db path")
    parser.add_argument('--host', default='localhost', help='Carla server hostname')
    parser.add_argument('--collision_type', default='LeftTurnCollision', help='collision type') # 交叉撞:RightAngleCollision 左轉穿越側撞:LeftTurnCollision
    parser.add_argument('--header', default='LeftTurnCollision100', help='output dir prefix')

    parser.add_argument('--anchor_name', default='traffic.speed_limit.100', help='object for observer anchor')
    parser.add_argument('--solid_actor_id', nargs='+', default=None, help='solid actor id')

    args = parser.parse_args()

    for value in np.random.uniform(0, args.time_offset_range, args.epochs):
        rpd = ReplayData(args.csv_path)
        rpd.set_time_offset(args.offset_actor_id, value)
        offset_label = str(value).replace('.', "_")
        rp = Replayer(args.host, rpd, f'{args.header}_{offset_label}', args.db_path, args.collision_type, args.anchor_name)

        if args.solid_actor_id is not None: 
            rp.solid_actors = args.solid_actor_id

        rp.play()
