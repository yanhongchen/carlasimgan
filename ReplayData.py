import pandas as pd
import numpy as np
from scipy import interpolate

class ReplayData:
    """
    load carla replay log file
    """

    def __init__(self, path):
        self.property_functions = {}
        self.control_functions = {}

        # Read file
        self.path = path
        self.df = pd.read_csv(self.path)
        
        # Sort by frame_count
        self.df = self.df.sort_values(by='frame_count')
    
        self.build()

    def build(self):
        self.start_elapsed_time = self.df["elapsed_seconds"].min()
        self.end_elapsed_time = self.df["elapsed_seconds"].max()

        self.df["elapsed_seconds"] -= self.start_elapsed_time

        self._build_property_functions()
        self._build_control_functions()

    def _check_file(self):
        pass

    def _get_by_actor_id(self, aid):
        """
        Get rows of selected actor
        """
        return self.df[self.df['actor_id'] == aid]

    def get_actor_type(self, aid):
        """
        Get actor type
        """
        return self._get_by_actor_id(aid)["actor_type"].unique()[0]

    def get_actor_role_name(self, aid):
        """
        Get actor role name
        """
        return self._get_by_actor_id(aid)["role_name"].unique()[0]

    def get_actor_color(self, aid):
        """
        Get color
        """
        r = self._get_by_actor_id(aid)["color_r"].to_numpy()[0]
        g = self._get_by_actor_id(aid)["color_g"].to_numpy()[0]
        b = self._get_by_actor_id(aid)["color_b"].to_numpy()[0]
        return r, g, b

    def set_time_offset(self, aid, value):
        """
        add start time offset
        """
        self.df.loc[self.df['actor_id'] == aid, "elapsed_seconds"] += value
        self.build()
    
    def set_vehicle_physics(self, aid, value):
        """
        """
        pass

    def set_throttle_ratio(self, aid, value):
        self.df.loc[self.df['actor_id'] == aid, "throttle"] *= value

    def get_actor_property(self, aid, target_eplapsed_time, parameter):
        f = self.property_functions[aid][parameter]
        try:
            return np.float(f(target_eplapsed_time))
        except ValueError as e:
            raise e
            '''
            df = self._get_by_actor_id(aid)
            _time = df["elapsed_seconds"].max() if target_eplapsed_time >= self.end_elapsed_time else df["elapsed_seconds"].min()
            print(e, target_eplapsed_time, "replace by", _time)
            value = f(_time)
            
            return np.float(value)
            '''

    def get_actor_control(self, aid, target_eplapsed_time, parameter):
        f = self.control_functions[aid][parameter]
        try:
            return np.float(f(target_eplapsed_time))
        except ValueError as e:
            raise e
            '''
            df = self._get_by_actor_id(aid)
            _time = df["elapsed_seconds"].max() if target_eplapsed_time >= self.end_elapsed_time else df["elapsed_seconds"].min()
            print(e, target_eplapsed_time, "replace by", _time)
            value = f(_time)
            return np.float(value)
            '''
    def get_actor_ids(self):
        aids = self.df["actor_id"].unique()
        return aids

    def _build_property_functions(self):
        """
        interpolation method for velocity, location, rotation, position
        """
        aids = self.df["actor_id"].unique()
        parameters = [
            "x", "y", "z",
            "pitch", "yaw", "roll",
            "velocity_x", "velocity_y", "velocity_z",
            ]
        for aid in aids:
            df = self._get_by_actor_id(aid)
            t = df["elapsed_seconds"].to_numpy()
            self.property_functions[aid] = {}
            for param in parameters:
                x = df[param].to_numpy()
                f = interpolate.interp1d(t, x, kind='linear', bounds_error=False, fill_value='extrapolate')
                self.property_functions[aid][param] = f
    
    def _build_control_functions(self):
        """
        interpolation method for velocity, location, rotation, position
        """
        aids = self.df["actor_id"].unique()
        parameters = [
            "throttle", "steer", "brake", # float
            "hand_brake", "reverse", "manual_gear_shift", # binary data
            "gear" #0, 1, 2, 3, 4
            ]
    
        for aid in aids:
            df = self._get_by_actor_id(aid)
            t = df["elapsed_seconds"].to_numpy()
            self.control_functions[aid] = {}
            for param in parameters:
                if param in ["throttle", "steer", "brake"]:
                    x = df[param].to_numpy()
                    f = interpolate.interp1d(t, x, kind='cubic', bounds_error=False, fill_value='extrapolate')
                else:
                    x = df[param].to_numpy()
                    f = interpolate.interp1d(t, x, kind='nearest', bounds_error=False, fill_value='extrapolate')
                
                self.control_functions[aid][param] = f

    def test(self):
        v = self.get_actor_control(114, 199, "gear")
        print(v)



if __name__ == '__main__':
    rd = ReplayData("20190826143229.csv")
    rd.test()
