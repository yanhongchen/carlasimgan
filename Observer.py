import glob, os, sys, math, csv, time, cv2, weakref, threading

try:
    sys.path.append(glob.glob('../../carla/dist/carla-*%d.%d-%s.egg' % (
        sys.version_info.major,
        sys.version_info.minor,
        'win-amd64' if os.name == 'nt' else 'linux-x86_64'))[0])
except IndexError:
    pass

import carla
from carla import ColorConverter as cc

import numpy as np
import xml.etree.ElementTree as ET
from BoundingBox import ClientSideBoundingBoxes
from DBHelper import DBHelper

class Observer:
    """
    target_actors: Actors we want to monitor.
    """

    def __init__(self, rgb_camera, seg_camera, video_options, csv_options, display_labels=False):
        self.display_labels = display_labels

        self.rgb_image = None
        self.seg_image = None

        self.rgb_camera = rgb_camera
        self.seg_camera = seg_camera

        self.video_options = video_options

        self.csv_fn = csv_options['filename']

        self.logs = {} 

        self.frames = {} 

        rgb_camera.listen(lambda image: self.rgb_camera_callback(image))    
        seg_camera.listen(lambda image: self.seg_camera_callback(image))

        self.start_frame = None
        self.server_frame = 0

    def capture(self, actor, server_frame):
        """
        call with command generator
        """
        '''
        while self.seg_image == None:
            time.sleep(0)
        '''
        if (self.rgb_image is not None) and (self.seg_image is not None):
            try:
                self.server_frame = server_frame #- self.start_frame
                if self.start_frame is None:
                    self.start_frame = self.server_frame

                self.server_frame -= self.start_frame

                log = self.captureActorLog(actor)

                frame = np.frombuffer(self.rgb_image.raw_data, dtype=np.uint8).reshape((self.rgb_image.height, self.rgb_image.width, 4))[:,:,:3].copy()
                box = log['position1x'], log['position1y'], log['position2x'], log['position2y']
                
                if str(actor.id) not in self.logs.keys():
                    self.logs[str(actor.id)] = []

                self.logs[str(actor.id)].append(log)
                
                if self.server_frame not in self.frames.keys():
                    self.frames[self.server_frame] = frame

                if not None in list(box) and self.display_labels:
                    draw_box(self.frames[self.server_frame], box, (255, 0, 0))

            except Exception as e:
                print(f"[log exception] {e}")

            
    def captureActorLog(self, actor):
        return self.get_bounding_box_log(actor, self.rgb_camera)

    def writeCSV(self):
        filename = self.csv_fn
        with open(filename, 'w', newline='') as csvfile:
            fieldnames = ['tracker', 'frame', 'labelName', 'position1x', 'position1y', 'position2x', 'position2y', 'center1x', 'center1y', 'realx', 'realy', 'angle', 'output', 'UpdateTime']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            for k in self.logs:
                for log in self.logs[k]:
                    writer.writerow(log)
    
    def writeVideo(self):
        fn = self.video_options['filename']
        print('saving {}'.format(fn))
        size = (self.video_options['width'], self.video_options['height'])
        videoWriter = cv2.VideoWriter(fn, cv2.VideoWriter_fourcc(*'DIVX'), 20, size)
        for i in self.frames.keys():
            videoWriter.write(self.frames[i])
        videoWriter.release()
        print('save {}'.format(fn))

    #@staticmethod
    def seg_camera_callback(self, image):
        '''
        self = weak_self()
        if not self:
            return
        '''
        if image is not None:
            self.seg_image = image


    #@staticmethod
    def rgb_camera_callback(self, image):
        '''
        self = weak_self()
        if not self:
            return
        '''
        if image is not None:
            self.rgb_image = image
            # write video
            #frame = np.frombuffer(self.rgb_image.raw_data, dtype=np.uint8).reshape((self.rgb_image.height, self.rgb_image.width, 4))[:,:,:3]
            #self.frames.append(frame.copy())

    def get_bounding_box_log(self, actor, camera):
        actor_transform = actor.get_transform()
        camera_location = camera.get_location()
        # init a data
        data = {}
        #if math.sqrt((actor_transform.location.x - camera_transform.location.x)**2 + (actor_transform.location.y - camera_transform.location.y)**2) < RANGE:
        RANGE = 200

        if camera_location.distance(actor_transform.location) < RANGE:
            bbox = ClientSideBoundingBoxes.get_bounding_box(actor, camera)
            npbbox = np.array(bbox)
            npbbox = npbbox[:,:2]
            npbbox = np.round(npbbox).astype(np.int32)
            #if True:#(npbbox > 0).all() and (npbbox[:,0] < int(self.camera.attributes['image_size_x'])).all() and (npbbox[:,1] < int(self.camera.attributes['image_size_y'])).all():
                #print(npbbox)

            data['position1x'] = None
            data['position1y'] = None
            data['position2x'] = None
            data['position2y'] = None
            data['center1x'] = None
            data['center1y'] = None

            if np.logical_and(np.logical_and((npbbox[:,0] > 0), (npbbox[:,0] < int(camera.attributes['image_size_x']))), np.logical_and((npbbox[:,1] > 0), (npbbox[:,1] < int(camera.attributes['image_size_y'])))).any():
                 self.add_bbox(npbbox, data)
            
            data['tracker'] = str(actor.id)
            data['labelName'] = "Sedan"
            '''
            data['velocity_x'] = actor.get_velocity().x
            data['velocity_y'] = actor.get_velocity().y
            data['velocity_z'] = actor.get_velocity().z
            '''
            data['realx'] = actor.get_location().x
            data['realy'] = actor.get_location().y
            #data['realz'] = actor.get_location().z
            data['frame'] = self.server_frame
            data['output'] = 0
            data['angle'] = None

        return data

    @staticmethod
    def decode_seg_image(image):
        npimg = np.frombuffer(image.raw_data, dtype=np.uint8).reshape((image.height, image.width, 4))
        npimg = npimg[:,:,2].squeeze()
        vehicle_map = (npimg == 10).astype(np.uint8) * 255
        ped_map = (npimg == 4).astype(np.uint8) * 255
        return vehicle_map, ped_map

    def add_bbox(self, _3dbox, data):
        xmin, ymin = _3dbox.min(axis=0)
        xmax, ymax = _3dbox.max(axis=0)
        xmin = max(0, xmin)
        ymin = max(0, ymin)
        xmax = min(self.seg_image.width-1, xmax)
        ymax = min(self.seg_image.height-1, ymax)

        segimg, _ = self.decode_seg_image(self.seg_image)
        subimg = segimg[ymin:ymax, xmin:xmax]
        nz = cv2.findNonZero(subimg)
        if nz is None:
            return

        cnts = cv2.findContours(subimg, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)[-2]
        
        if len(cnts) > 0:
            max_cnt = max(cnts, key=cv2.contourArea)
            if cv2.contourArea(max_cnt) < (xmax-xmin) * (ymax-ymin) / 2:
                cnt = nz
            else:
                cnt = max_cnt
        else:
                cnt = None
                
        if cnt is not None and len(cnt) > 0:
            x, y, w, h = cv2.boundingRect(cnt)
            #print(x, y, w, h)
            bbox_xmin = xmin + x
            bbox_ymin = ymin + y
            bbox_xmax = bbox_xmin + w
            bbox_ymax = bbox_ymin + h

            data['position1x'] = int(bbox_xmin)
            data['position1y'] = int(bbox_ymin)
            data['position2x'] = int(bbox_xmax)
            data['position2y'] = int(bbox_ymax)

            data['center1x'] = (data['position1x'] + data['position2x']) // 2
            data['center1y'] = (data['position1y'] + data['position2y']) // 2

    def destroy(self):
        self.rgb_camera.destroy()
        self.seg_camera.destroy()


def draw_box(image, box, color, thickness=3):
    b = np.array(box).astype(int)
    cv2.rectangle(image, (b[0], b[1]), (b[2], b[3]), color, thickness, cv2.LINE_AA)
