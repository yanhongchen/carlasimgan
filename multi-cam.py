
from __future__ import print_function


# ==============================================================================
# -- find carla module ---------------------------------------------------------
# ==============================================================================


import glob
import os
import sys

try:
    sys.path.append(glob.glob('../carla/dist/carla-*%d.%d-%s.egg' % (
        sys.version_info.major,
        sys.version_info.minor,
        'win-amd64' if os.name == 'nt' else 'linux-x86_64'))[0])
except IndexError:
    pass


# ==============================================================================
# -- imports -------------------------------------------------------------------
# ==============================================================================


import carla

from carla import ColorConverter as cc

def process_image(image):
    image.convert(cc.Raw)
    image.save_to_disk("_out/%08d" % image.frame_number)

def process_image2(image):
    image.convert(cc.Raw)
    image.save_to_disk("_out2/%08d" % image.frame_number)

client = carla.Client('172.20.0.13', 2000)
client.set_timeout(2.)

world = client.get_world()

#settings = world.get_settings()
#settings.synchronous_mode = True
#world.apply_settings(settings)

spectator = world.get_actors().filter('spectator')[0]
actors = world.get_actors()
print(actors)
anchor = actors.filter('traffic.speed_limit.90')[0]
bp_library = world.get_blueprint_library()

camera_bp = bp_library.find('sensor.camera.rgb')
camera_bp.set_attribute("image_size_x", '640')
camera_bp.set_attribute("image_size_y", '360')

camera_bp2 = bp_library.find('sensor.camera.rgb')
camera_bp2.set_attribute("image_size_x", '640')
camera_bp2.set_attribute("image_size_y", '360')


old_transform = spectator.get_transform()
new_transform = anchor.get_transform() 

camera = world.spawn_actor(camera_bp, old_transform)
camera.listen(process_image)

camera2 = world.spawn_actor(camera_bp2, new_transform)
camera2.listen(process_image2)

actors = world.get_actors().filter('*')
for actor in actors:
    print(actor, actor.get_transform())

try:
    while True:
        world.wait_for_tick()
except KeyboardInterrupt:
    camera.destroy()
    print("Exit")


'''
actors = world.get_actors().filter('*')
print(actors)
for actor in actors:
    print(actor, actor.get_transform())
'''
